# Braess' Paradox
This is a python module containing code relevant to studying Braess edges and the Braess closure of graphs.
This project uses conda environments to add and install dependencies, please refer to Conda's
[install guide](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) if you do not
have conda installed.

## Installation
To download and install the module and its dependencies, clone the repository and create a new conda environment
from the .yml file in the root directory:

```bash
$ git clone https://gitlab.com/spectral-graph-theory/braessparadox
$ conda env create -f braessparadox/braessparadox.yml
```

## Usage
After installation, activate the conda environment and import the module in python

```bash
$ conda activate braessparadox
$ python3
```
```python
>>> import braessparadox as bp
```
