from .resistance import *

# Aliases for functions
rdist   = resistance_distance
maxres  = max_resistance
trees   = spanning_trees
forests = spanning_forests
kind    = kirchhoff_index
kconst  = kemenys_constant

__all__ = ['resistance_distance', 'rdist', 'max_resistance', 'maxres',
           'spanning_trees', 'trees', 'spanning_forests', 'forests',
            'kemenys_constant', 'kconst', 'is_braess', 'braess_edges',
           'braess_closure', 'resistance_matrix']
__title__ = 'braess-paradox'
__version__ = '0.1.0'
__license__ = 'GNU GPLv3'
