from networkx import Graph

def separate(G: Graph, u):
    """
    Separates G into two components using u as a separator

    Parameters
    ----------
    G : NetworkX Graph
        The graph to be separated
    u : Any
        The vertex to use as the separator

    Returns
    -------
    G_1, G_2 : networkx.Graph
        The components of the separation

    Raises
    ------
    ValueError
        If u is not a vertex in G
    """
    if not G.has_node(u):
        raise ValueError('u must be in the vertex set of G')

    G_1 = Graph()
    G_2 = Graph()

    return G_1, G_2
