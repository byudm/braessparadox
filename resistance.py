from math import trunc
from array import array
from fractions import Fraction

from networkx import Graph
from networkx import laplacian_matrix as lapl
from networkx import adjacency_matrix as adjmx
from numpy import diagflat, ndarray, float64
from scipy.linalg import det, inv, pinv, eigvals

def resistance_distance(G: Graph, i, j) -> float:
    """
    Computes the resistance between i and j in G.

    Parameters
    ----------
    G : NetworkX Graph
        The graph containing i and j
    i, j :
        One of the vertices to compute resistance between

    Returns
    -------
    res : float
        The effective resistance between i and j

    Raises
    ------
    ValueError
        If i or j is not in the vertex set of G
    """
    V = G.nodes()
    u = None
    v = None

    for (_i, _v) in enumerate(V):
        if _v == i:
            u = _v
        if _v == j:
            v = _v

    if u == None or v == None:
        raise ValueError('i or j is not in the vertex set of G')
    elif u == v:
        return 0


    L = lapl(G).todense()
    M = pinv(L)

    return M[u, u] + M[v, v] - M[u, v] - M[v, u]

def max_resistance(G: Graph) -> (float, tuple):
    """
    Returns the maximum resistance of G and the vertex pair across
    which the maximum occurs.

    Parameters
    ----------
    G : NetworkX Graph
        The graph to find the max resistance in

    Returns
    -------
    max : float
        The maximum resistance
    vertices : tuple
        The vertex pair over which the resistance is maximized
    """
    L = lapl(G).todense()
    M = pinv(L)
    V = G.nodes()
    max = -1.0
    vertices = None

    for (i, u) in enumerate(V):
        for (j, v) in enumerate(V):
            if u == v:
                continue
            else:
                res = M[i, i] + M[i, j] - M[i, j] - M[j, i]
                if res > max:
                    max = res
                    vertices = (u, v)

    return max, vertices

def spanning_trees(G: Graph) -> int:
    """
    Returns the approximate number of spanning trees in G.

    Parameters
    ----------
    G : networkx Graph
        The graph to compute the number of spanning trees of

    Returns
    -------
    trees : int
        The number of spanning trees in G
    """
    N = G.number_of_nodes()
    L = lapl(G)[1:N, 1:N].todense()

    return trunc(det(L))

def spanning_forests(G: Graph, *argv) -> int:
    """
    Returns the number of spanning n-forests separating
    v_1, v_2, ..., v_n in G.

    Parameters
    ----------
    G : networkx.Graph
        The graph containing v_1, v_2, ..., v_n

    *argv : tuple
        The set of vertices to find the number of spanning n-forests between.

    Returns
    -------
    trees: int
        The number of spanning forests separating v_1, v_2, ..., v_n
    """
    n = len(argv)
    allunique = True

    for i in range(0, n-1):
        if not G.has_node(argv[i]):
            raise ValueError('')
        for j in range(i+1, n-1):
            if not allunique:
                break
            elif i == j:
                allunique = False
                break

    if not allunique:
        return 0

    slice = array('i')

    for (_i, _v) in enumerate(G.nodes()):
        if _v not in argv:
            slice.append(_i)

    L = lapl(G)[slice,:][:,slice].todense()
    return trunc(det(L))

def kirchhoff_index(G: Graph) -> float:
    """
    Computes the kirchoff index of G.

    Parameters
    ----------
    G : NetworkX Graph
        The graph to compute the kirchoff index of

    Returns
    -------
    kind : float
        The kirchoff index of G
    """
    L = lapl(G).todense()
    M = pinv(L)

    G.number_of_nodes() * M.trace()

def kemenys_constant(G: Graph) -> float:
    """
    Computes Kemeny's constant for G.

    Parameters
    ----------
    G : NetworkX Graph
        The graph to compute Kemeny's constant of

    Returns
    -------
    kconst : float
        Kemeny's constant of G
    """
    D = diagflat([d[1] for d in G.degree()])
    M = inv(D) * adjmx(G)
    l = eigvals(M)

    kconst = 0
    for i in l:
        if abs(i.real - 1.0) > 2e-6:
            kconst += 1/(1-i.real)

    return kconst

def is_braess(G: Graph, i, j) -> bool:
    """
    Returns true if (i, j) is a braess edge of G.

    Parameters
    ----------
    G : NetworkX Graph
        The graph containing i and j
    i, j : 
    """
    if not G.has_node(i) or not G.has_node(j):
        raise ValueError('G must contain both i and j')
    elif G.has_edge(i, j):
        return False

    H = G.copy()
    H.add_edge(i, j)

    return kemenys_constant(H) > kemenys_constant(G)

def braess_edges(G: Graph) -> list:
    """
    Returns a list of braess edges in G

    Parameters
    ----------
    G : NetworkX Graph
        The graph to find braess edges in

    Returns
    -------
    edges : list
        A list of braess edges in G
    """
    V = list(G.nodes())
    n = G.number_of_nodes()
    edges = list()

    for i in range(0, n-1):
        for j in range(i+1, n-1):
            if is_braess(G, V[i], V[j]):
                edges.append((V[i], V[j]))

    return edges

def braess_closure(G: Graph) -> Graph:
    """
    Returns the braess closure of G

    Parameters
    ----------
    G : NetworkX Graph
        The graph to find the braess closure of

    Returns
    -------
    H : NetworkX Graph
        The braess closure of G
    """
    H = G.copy()
    V = H.nodes()
    n = H.number_of_nodes()

    for i in range(0, n-1):
        for j in range(i+1, n-1):
            if is_braess(H, V[i], V[j]):
                H.add_edge(V[i], V[j])

    return H

def resistance_matrix(G: Graph) -> ndarray:
    """
    Returns the resistance matrix of G.

    Parameters
    ----------
    G : NetworkX Graph
        The graph to compute the resistance matrix of

    Returns
    -------
    R : NumPy array
        The matrix whose (i, j)-th entry is $r_G(i, j)$
    """
    L = lapl(G).todense()
    M = pinv(L)
    n = G.number_of_nodes()
    R = ndarray((n,n), dtype=float64)

    for i in range(0, n-1):
        for j in range(0, n-1):
            if j == i:
                R[i, j] = 0
            else:
                R[i, j] = M[i, i] + M[j, j] - M[i, j] - M[j, i]

    return R
